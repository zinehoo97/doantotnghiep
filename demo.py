import tensorflow as tf
import cv2


def feature_extractor(x):
    with tf.variable_scope("feature_extractor", reuse=tf.AUTO_REUSE):
        conv0 = tf.layers.conv2d(inputs=x, filters=32, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv0 = tf.layers.conv2d(inputs=conv0, filters=32, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        #shape = (N, 128, 128, 32)
        pool0 = tf.layers.max_pooling2d(inputs=conv0, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 64, 64, 32)

        conv1 = tf.layers.conv2d(inputs=pool0, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv1 = tf.layers.conv2d(inputs=conv1, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 64, 64, 64)
        pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 32, 32, 64)

        conv2 = tf.layers.conv2d(inputs=pool1, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv2 = tf.layers.conv2d(inputs=conv2, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 32, 32, 128)
        pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 16, 16, 128)

        conv3 = tf.layers.conv2d(inputs=pool2, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv3 = tf.layers.conv2d(inputs=conv3, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 16, 16, 256)
        pool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 8, 8, 256)

        conv4 = tf.layers.conv2d(inputs=pool3, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv4 = tf.layers.conv2d(inputs=conv4, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 8, 8, 512)
        pool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 4, 4, 512)

        conv5 = tf.layers.conv2d(inputs=pool4, filters=1024, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv5 = tf.layers.conv2d(inputs=conv5, filters=1024, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 4, 4, 1024)

        return conv0, conv1, conv2, conv3, conv4, conv5


def generator(x, shirt_conv0, shirt_conv1, shirt_conv2, shirt_conv3, shirt_conv4):
    """
    Generate final output, based U-net
    :param inputs: Mixed feature, shape = (N, 16, 16, 512), dtype = float
    :return: Result image RGB , shape = (N, 128, 128, 3), dtype = float
    """
    with tf.variable_scope("generator", reuse=tf.AUTO_REUSE):
        conv0 = tf.layers.conv2d(inputs=x, filters=32, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv0 = tf.layers.conv2d(inputs=conv0, filters=32, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 128, 128, 32)
        pool0 = tf.layers.max_pooling2d(inputs=conv0, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 64, 64, 32)

        conv1 = tf.layers.conv2d(inputs=pool0, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv1 = tf.layers.conv2d(inputs=conv1, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        #shape = (N, 64, 64, 64)
        pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=(2,2), strides=(2,2), padding="SAME")
        #shape = (N, 32, 32, 64)

        conv2 = tf.layers.conv2d(inputs=pool1, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv2 = tf.layers.conv2d(inputs=conv2, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 32, 32, 128)
        pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 16, 16, 128)

        conv3 = tf.layers.conv2d(inputs=pool2, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv3 = tf.layers.conv2d(inputs=conv3, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 16, 16, 256)
        pool3 = tf.layers.max_pooling2d(inputs=conv3, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 8, 8, 256)

        conv4 = tf.layers.conv2d(inputs=pool3, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv4 = tf.layers.conv2d(inputs=conv4, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 8, 8, 512)
        pool4 = tf.layers.max_pooling2d(inputs=conv4, pool_size=(2, 2), strides=(2, 2), padding="SAME")
        # shape = (N, 4, 4, 512)

        conv5 = tf.layers.conv2d(inputs=pool4, filters=1024, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        conv5 = tf.layers.conv2d(inputs=conv5, filters=1024, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        # shape = (N, 4, 4, 1024)

        de_conv4 = tf.layers.conv2d_transpose(inputs=conv5, filters=512, kernel_size=(2,2), strides=(2,2), padding="SAME", activation=tf.nn.relu)
        #shape = (N, 8, 8, 512)
        concat4 = tf.concat((de_conv4, shirt_conv4), axis=3)
        #shape = (N, 8, 8, 1024)
        up_conv4 = tf.layers.conv2d(inputs=concat4, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        up_conv4 = tf.layers.conv2d(inputs=up_conv4, filters=512, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                 activation=tf.nn.relu)
        #shape = (N, 8, 8, 512)

        de_conv3 = tf.layers.conv2d_transpose(inputs=up_conv4, filters=256, kernel_size=(2, 2), strides=(2, 2),
                                              padding="SAME", activation=tf.nn.relu)
        # shape = (N, 16, 16, 256)
        concat3 = tf.concat((de_conv3, shirt_conv3), axis=3)
        # shape = (N, 16, 16, 512)
        up_conv3 = tf.layers.conv2d(inputs=concat3, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        up_conv3 = tf.layers.conv2d(inputs=up_conv3, filters=256, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        #shape = (N, 16, 16, 256)

        de_conv2 = tf.layers.conv2d_transpose(inputs=up_conv3, filters=128, kernel_size=(2, 2), strides=(2, 2),
                                              padding="SAME", activation=tf.nn.relu)
        # shape = (N, 32, 32, 128)
        concat2 = tf.concat((de_conv2, shirt_conv2), axis=3)
        # shape = (N, 32, 32, 256)
        up_conv2 = tf.layers.conv2d(inputs=concat2, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        up_conv2 = tf.layers.conv2d(inputs=up_conv2, filters=128, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        #shape = (N, 32, 32, 128)

        de_conv1 = tf.layers.conv2d_transpose(inputs=up_conv2, filters=64, kernel_size=(2, 2), strides=(2, 2),
                                              padding="SAME", activation=tf.nn.relu)
        # shape = (N, 64, 64, 64)
        concat1 = tf.concat((de_conv1, shirt_conv1), axis=3)
        # shape = (N, 64, 64, 128)
        up_conv1 = tf.layers.conv2d(inputs=concat1, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        up_conv1 = tf.layers.conv2d(inputs=up_conv1, filters=64, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        #shape = (N, 64, 64, 64)

        de_conv0 = tf.layers.conv2d_transpose(inputs=up_conv1, filters=32, kernel_size=(2, 2), strides=(2, 2),
                                              padding="SAME", activation=tf.nn.relu)
        # shape = (N, 128, 128, 32)

        concat0 = tf.concat((de_conv0, shirt_conv0), axis=3)
        up_conv0 = tf.layers.conv2d(inputs=concat0, filters=32, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        up_conv0 = tf.layers.conv2d(inputs=up_conv0, filters=3, kernel_size=(3, 3), strides=(1, 1), padding="SAME",
                                    activation=tf.nn.relu)
        #shape = (N, 128, 128, 3)

        return up_conv0


import time
import os
import random
data_shirt = [x for x in os.listdir("./sample_shirt") if ".jpg" in x]
data_user = [x for x in os.listdir("./sample_user_preprocessed") if ".png" in x]


input_cloth_image = tf.placeholder(dtype=tf.float32, shape=[None, 128, 128, 3], name="input_cloth_image")
input_user_image = tf.placeholder(dtype=tf.float32, shape=[None, 128, 128, 3], name="input_user_image")
input_generator = tf.concat((input_cloth_image, input_user_image), axis=3)
shirt_con0, shirt_con1, shirt_con2, shirt_con3, shirt_con4, _ = feature_extractor(input_cloth_image)
fake_image = generator(input_generator, shirt_con0, shirt_con1, shirt_con2, shirt_con3, shirt_con4)

config = tf.ConfigProto()
config.gpu_options.allow_growth = True

with tf.Session(config=config) as sess:
    sess.run(tf.global_variables_initializer())
    saver = tf.train.Saver()
    saver.restore(sess, "./doantotnghiep_model/model_510.ckpt")

    while True:
        user_id_image = random.choice(data_user)
        user_image = cv2.imread('./sample_user_preprocessed/' + user_id_image)
        # user_image = user_image[0:300, 0:351]
        user_image = cv2.resize(user_image, (128, 128))
        user_image = cv2.cvtColor(user_image, cv2.COLOR_BGR2RGB)
        user_image = user_image / 255.0

        cloth_image = cv2.imread("./sample_shirt/" + random.choice(data_shirt))
        cloth = cloth_image.copy()
        cloth_image = cv2.cvtColor(cloth_image, cv2.COLOR_BGR2RGB)
        cloth_image = cv2.resize(cloth_image, (128, 128))
        cloth_image = cloth_image / 255.0

        t = time.time()
        fake_img = sess.run([fake_image], feed_dict={input_user_image:[user_image], input_cloth_image:[cloth_image]})
        print("Processing time: ", time.time()-t)
        rs = fake_img[0][0]

        rs = cv2.cvtColor(rs, cv2.COLOR_RGB2BGR)


        result = cv2.resize(rs, (256, 256))
        cloth = cv2.resize(cloth, (256, 256))
        cv2.imshow("hjhj", result)
        cv2.imshow("hjhjhj", cloth)
        cv2.waitKey()