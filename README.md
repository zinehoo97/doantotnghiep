# Applying machine learning into clothes try-on



## Requirement
```
Python 3.6
Tensorflow 1.14
Opencv 4.1.1
```

## How to run inference script

### Clone this repository:
```
git clone https://github.com/zinehoo97/doantotnghiep.git
cd ./doantotnghiep
```

### Run demo.py
This script will use user's images in ./sample_user_preprocessed/ folder and the shirts in ./sample_shirt/ folder
```
python3 demo.py
```

